import { Application, Context } from "https://deno.land/x/abc@v1.0.0-rc10/mod.ts";
import { getBooks, getBook, addBook, deleteBook } from "./controllers/BookController.ts";

const app: Application = new Application();

app.static('/', './public');

app
    .get('/books', getBooks)
    .get('/book/:id', getBook)
    .post('/book', addBook)
    .delete('/book/:id', deleteBook)

// routes
app.get('/', async (ctx: Context) => {
    await ctx.file('./public/index.html');
})

// listen to port
app.start({ port: 3000 });
