import { Context } from "https://deno.land/x/abc@v1.0.0-rc10/mod.ts";
import { v4 } from "https://deno.land/std/uuid/mod.ts";
import { Book } from "../model/Book.ts";

let books: Book[] = [
    { id: v4.generate(), title: 'Pemrograman Web dengan Node.js dan JavaScript', author: 'Budi Raharjo', pages: 256 },
    { id: v4.generate(), title: 'Eloquent JavaScript', author: 'Brad Traversy', pages: 192 },
    { id: v4.generate(), title: 'Introduction to Deno', author: 'Maximilian Schwarzmuller', pages: 32 },
];

export const getBooks = (ctx: Context) => {
    ctx.json(books, 200);
}


export const getBook = (ctx: Context) => {
    const { id } = ctx.params;
    const book = books.find((b: Book) => b.id == id);
    
    if (book) {
        return ctx.json(book);
    }

    return ctx.json({ message: "Book not found!" }, 404);
}


export const addBook = async (ctx: Context) => {
    const id: string = v4.generate();
    const { title, author, pages } = await ctx.body();
    const book = { id, title, author, pages };
    books.push(book);

    return ctx.json(book, 201);
}


export const deleteBook = (ctx: Context) => {
    const { id } = ctx.params;
    const book = books.find((b: Book) => b.id == id);
    
    if (book) {
        books = books.filter((b: Book) => b.id != id);
        return ctx.json(book, 200);
    }

    return ctx.json({ message: "Book not found!" }, 404);
}

