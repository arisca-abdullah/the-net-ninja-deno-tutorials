import { serve, Server } from "https://deno.land/std/http/server.ts";

const server: Server = serve({ port: 3000 });
console.log('Server listen on http://localhost:3000');

for await (const req of server) {
    const url = `http://localhost:3000${req.url}`;
    console.log(`User visited ${url}`);
    req.respond({
        body: `
                <h1>Hello, World!</h1>
                <p>You accessed ${url}</p>
            `,
    });
}
