// fs module
import { readJson, writeJson } from "https://deno.land/std/fs/mod.ts";

// write JSON file
const user: object = {
    name: 'Arisca Abdullah',
    email: 'arisca.abdullah08@gmail.com',
    linkedin: 'https://www.linkedin.com/in/arisca-abdullah/',
};

await writeJson('user.json', user, { spaces: 2 });

// read JSON file
const res: unknown = await readJson('user.json');
console.log(res);
