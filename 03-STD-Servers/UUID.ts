// uuid module
import { v4 } from "https://deno.land/std/uuid/mod.ts";

// Generate a v4 uuid
const uid: string = v4.generate();
if (v4.validate(uid)) {
    console.log(uid);
}
