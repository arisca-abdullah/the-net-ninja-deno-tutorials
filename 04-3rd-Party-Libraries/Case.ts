import { camelCase, paramCase, pascalCase, snakeCase } from "https://deno.land/x/case/mod.ts";

const text: string = 'hello world';

console.log(`
Camel Case : ${camelCase(text)}
Param Case : ${paramCase(text)}
Pascal Case: ${pascalCase(text)}
Snake Case : ${snakeCase(text)}
`);
