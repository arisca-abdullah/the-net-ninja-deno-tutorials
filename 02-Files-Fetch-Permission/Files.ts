// Write File
const encoder: TextEncoder = new TextEncoder();
const data: string = 'Hello, World! ';

await Deno.writeFile('readme.txt', encoder.encode(data));

// Read File
const decoder: TextDecoder = new TextDecoder('utf-8');
const content: Uint8Array = await Deno.readFile('readme.txt');

console.log(decoder.decode(content));

// Read Text File
const text: string = await Deno.readTextFile('readme.txt');
console.log(text);

// Rename File
await Deno.rename('readme.txt', 'deleteme.txt');

// Delete File
await Deno.remove('deleteme.txt');